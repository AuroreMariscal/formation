#! bin/python2.7
# encoding: utf-8
import os
import random
import unicodedata


class Pendu(object):
    """
    Jeu du Pendu
    """
    def __init__(self):
        self.state = 1
        self.luck = 8
        self.lucky = self.luck
        self.path = './users'
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        with open("dictionary.txt", "r") as dictionary:
            self.dictionary = dictionary.read().split("\n")
            del self.dictionary[-1]
        self.score = 0
        self.file_name = None
        self.word = None
        self.word_response = []
        self.file_user = None
        self.user = None

    def login(self):
        while self.state == 1:
            self.user = raw_input("Comment vous appelez-vous ? ")
            format_user = unicodedata.normalize('NFKD', unicode(self.user, 'utf-8')).encode('ASCII', 'ignore').replace('-', '')
            self.file_name = "%s.txt" % format_user.lower()
            self.file_user = "%s/%s" % (self.path, self.file_name)
            if format_user.isalpha():
                if self.file_name not in os.listdir(self.path):
                    file_user = open(self.file_user, "w")
                    file_user.close()
                self.state = 2
                break
            else:
                print "Votre nom ne peut contenir que des lettres"
                continue

    def start(self):
            file_user = open(self.file_user, "r")
            self.score = file_user.read()
            file_user.close()
            if not self.score:
                self.score = 0
            self.word = unicodedata.normalize('NFKD', unicode(random.choice(self.dictionary), 'utf-8')).encode('ASCII', 'ignore')
            self.word_response = ['*' for i in self.word]
            msg = "Bonjour %s, essayez de trouver le mot suivant : %s" % (self.user.capitalize(), ''.join(self.word_response))
            printmessage(msg, '-')
            print "Vous avez %s essais et votre score est actuellement de %s" % (self.luck, self.score)

    def game(self):
        while self.state == 2:
            letter = raw_input("\nChoisissez une lettre de a à z : ")
            if letter.isalpha() and len(letter) == 1:
                list_letter_word = list(enumerate(list(self.word), 0))
                result = 0
                for index, letter_word in list_letter_word:
                    if letter == letter_word:
                        self.word_response[index] = letter
                        result += 1
                if result > 0:
                    msg = "Bien joué : %s" % ''.join(self.word_response)
                else:
                    self.lucky -= 1
                    msg = "Mauvaise lettre : %s" % ''.join(self.word_response)

                printmessage(msg, '-')
                print "Il vous reste %s essais" % self.lucky

                if self.lucky < 1:
                    self.state = 3
                    msg = "PENDU!!!"
                    printmessage(msg, '=')
                    self.replay()

                if self.word_response.count('*') == 0:
                    self.score = self.lucky + int(self.score)
                    msg = "BRAVO!!! Vous avez gagné %s point(s) et votre score est donc de %s point(s)" % (self.lucky, self.score)
                    printmessage(msg, '=')
                    file_user = open(self.file_user, "w")
                    file_user.write(str(self.score))
                    file_user.close()
                    self.state = 3
                    self.replay()

            else:
                print "Vous devez tapez une lettre"
                continue

    def replay(self):
        while self.state == 3:
            self.lucky = self.luck
            self.word_response = []
            choice_game = raw_input("Pour rejouer, tapez 1, pour quitter, tapez 2 : ")
            if not choice_game.isdigit():
                print "Vous devez choisir entre 1 ou 2\n"
                continue
            elif int(choice_game) != 1 and int(choice_game) != 2:
                print "Vous devez choisir entre 1 ou 2\n"
                continue
            else:
                if int(choice_game) == 1:
                    self.start()
                    self.state = 2
                    break
                else:
                    break

    def main(self):
        print """
                  ==========================\n
                  Bienvenue sur le jeu Pendu\n
                  ==========================
              """
        self.login()
        self.start()
        self.game()


def printmessage(msg, caract):
    ligne = ''.join([caract for i in msg])
    print """
    %s
    %s
    %s
    """ % (ligne, msg, ligne)

if __name__ == '__main__':
    """
    """
    Pendu().main()
