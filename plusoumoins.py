#! bin/python2.7
# encoding: utf-8
from random import uniform


class Level():
    def __init__(self, str_niveau, nbr_min=0, nbr_max=10, nbr_float=0):
        self.str_niveau = str_niveau
        self.nbr_min = nbr_min
        self.nbr_max = nbr_max
        self.nbr_float = nbr_float


class PlusOuMoins(object):
    """
    Jeu du plus ou moins
    """
    def __init__(self):
        self.random_result = None
        self.compteur = 0
        self.choix_niveau = None
        self.choix_chiffre = None
        self.level = None
        self.levels = [Level('Facile', 0, 10, 0),
                       Level('Moyen', 0, 25, 0),
                       Level('Difficile', 0, 10, 1)]

    def compare_nombre(self):
        self.compteur += 1
        s = ""
        eme = "er"
        if self.compteur > 1:
            s = "s"
            eme = "eme"
        if self.choix_chiffre == self.random_result:
            print """
            =====================\n
            Gagné en {} coup{} !!!\n
            =====================\n
            """.format(str(self.compteur), s)
            self.compteur = 0
            return False
        if self.choix_chiffre < self.random_result:
            print "Plus grand"
        if self.choix_chiffre > self.random_result:
            print "Plus petit"
        print "C'est votre {}{} coup{}, réessayez!!!".format(str(self.compteur), eme, s)
        return True

    def test_level(self):
        msg = ["\nChoisissez votre niveau, en tapant : "]
        for levels in self.levels:
            msg_level = "{nbr_niveau} pour le niveau {str_niveau}".format(nbr_niveau=levels[0],
                                                                          str_niveau=levels[1].str_niveau)
            msg.append(msg_level)
        msg.append("Niveau choisi : ")
        choix_niveau = raw_input("\n".join(msg))
        if not choix_niveau.isdigit():
            print "Vous devez taper un chiffre"
            return False
        self.choix_niveau = int(choix_niveau)

        if self.choix_niveau > len(self.levels):
            print "Le niveau choisi n'existe pas"
            return False
        self.level = self.levels[self.choix_niveau - 1][1]
        return True

    def test_nombre(self):
        choix_chiffre = raw_input("\nNombre choisi : ")
        if not choix_chiffre.replace('.', '', 1).isdigit():
            return True

        self.choix_chiffre = float(choix_chiffre)
        if self.choix_chiffre > self.level.nbr_max or self.choix_chiffre < self.level.nbr_min:
            print "Le nombre choisi n'est pas compris entre {} et {}".format(self.level.nbr_min, self.level.nbr_max)
            return True

    def random(self):
        self.random_result = round(uniform(self.level.nbr_min, self.level.nbr_max), self.level.nbr_float)

    def instruction(self):
                msg = ["\nChoisissez un chiffre de {} à {}".format(self.level.nbr_min, self.level.nbr_max)]
                if self.level.nbr_float == 1:
                    msg.append("avec 1 chiffre après la virgule")
                if self.level.nbr_float > 1:
                    msg.append("avec {nbr_float} chiffres après la virgule".format(nbr_float=self.level.nbr_float))
                print " ".join(msg)

    def custom(self):
        NotImplemented

    def main(self):
        print """
                  ========================================\n
                  Bienvenue sur le jeu du Plus ou du Moins\n
                  ========================================
              """
        self.levels = list(enumerate(self.levels, 1))
        while True:
            self.custom()
            if not self.test_level():
                continue

            else:
                self.random()
                self.instruction()
                while True:
                    if self.test_nombre():
                        continue
                    else:
                        if not self.compare_nombre():
                            break


if __name__ == '__main__':
    """
    On instancie la classe de cette manière afin que le script ne soit lancé que si on l'appelle directement.
    Car si on l'instanciait en dehors de toute fonction, le script serait directment lancé à chaque import par exemple.
    """
    PlusOuMoins().main()
