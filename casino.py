#! bin/python2.7
# encoding: utf-8
from random import randrange


class Casino:
    """
    Ce script simule un jeu de casino
    """
    def __init__(self):
        self.random_min = 0
        self.random_max = 50
        self.mise = 0
        self.argent_reserve = 100
        self.argent_mise = 0
        self.resultat = 0

    def main(self):
        msg = None
        lettre = raw_input("Tapez 'Y' pour jouer, 'Q' pour quitter : ")
        if str(lettre) == 'Y' or str(lettre) == 'y':
            while self.argent_reserve > 0:
                print "Vous avez ", self.argent_reserve, "euro en poche"

                argent_mise = raw_input("Quelle est votre mise : ")
                if not argent_mise.isdigit():
                    print "Mettez un chiffre!!!"
                    continue
                self.argent_mise = int(argent_mise)
                reste = self.argent_reserve - self.argent_mise
                if reste < 0:
                    print "T'es un peu clodo et t'as pas assez d'argent pour miser autant? Mise plus petit alors!"
                    continue

                else:
                    mise = raw_input("Choisissez un chiffre de 1-50 : ")
                    if not mise.isdigit():
                        print "Mettez un chiffre!!!"
                        continue
                    self.mise = int(mise)
                    if self.mise < 1 or self.mise > 50:
                        print "Le chiffre doit être compris entre 1-50"
                        continue
                    self.resultat = self.lancement_roulette()
                    egalite = self.compare_egalite()
                    if not egalite:
                        paire = self.compare_paire()
                        if not paire:
                            self.argent_reserve -= self.argent_mise
                            print "Dommage, vous avez perdu. Il vous reste ", self.argent_reserve, ' euros'

            msg = "Vous êtes ruiné, vous n'avez plus assez d'argent pour jouer"

        elif lettre == 'Q' or lettre == 'q':
            msg = "Au revoir"

        else:
            msg = "Mauvais touche"

        print msg

    def lancement_roulette(self):
        if self.mise % 2:
            mise_pair = "Rouge."
        else:
            mise_pair = "Noir."
        print "\nVous avez choisi le :", self.mise, mise_pair
        print "\nLes jeux sont faits..."
        self.resultat = randrange(self.random_min, self.random_max)
        if self.resultat % 2:
            resultat_pair = "Rouge."
        else:
            resultat_pair = "Noir."
        print "\nEt ce sera... Un :", self.resultat, resultat_pair
        return self.resultat

    def compare_egalite(self):
        if self.mise == self.resultat:
            gain = self.argent_mise * 3
            self.argent_reserve += gain
            print 'Bravo, vous avez bien misé, votre compte est crédité de + ', gain, "\n"
            return True
        else:
            return False

    def compare_paire(self):
        if (self.mise % 2) == (self.resultat % 2):
            gain = round(self.argent_mise / 2.0, 2)
            self.argent_reserve += gain
            print 'Bravo, vous avez misé la bonne couleur, votre compte est crédité de + ', gain, "\n"
            return True
        else:
            return False


if __name__ == '__main__':
    """
    On instancie la classe de cette manière afin que le script ne soit lancé que si on l'appelle directement.
    Car si on l'instanciait en dehors de toute fonction, le script serait directment lancé à chaque import par exemple.
    """
    Casino().main()
